package com.epam.binaryTree;

import java.util.logging.Level;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class Appliation {

    public static void main(String[] args) {
        BinaryTree theTree = new BinaryTree();
        theTree.addNode(50, "Boss");
        theTree.addNode(25, "Vice Pres");
        theTree.addNode(15, "Office Manager");
        theTree.addNode(30, "Secretary");
        theTree.addNode(75, "Sales Manager");
        theTree.addNode(85, "Salesman 1");
        theTree.inOrderTraverseTree(theTree.root);
        LOGGER.log(Level.INFO, "Preorder");
        theTree.preorderTraverseTree(theTree.root);
        LOGGER.log(Level.INFO, "Postorder");
        theTree.postorderTraverseTree(theTree.root);
        LOGGER.log(Level.INFO, "Search for 30");
        System.out.println(theTree.findNode(30));
        LOGGER.log(Level.INFO, "Удаляем ел 25");
        theTree.remove(25);
        theTree.preorderTraverseTree(theTree.root);


    }
}
